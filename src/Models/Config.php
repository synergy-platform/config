<?php

namespace Synergy\Config\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Eloquent model representing the "config" table
 *
 * Part of the Config package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Config
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/config
 */

class Config extends Model
{
	/**
	 * {@inheritDoc}
	 */
	protected $table = 'config';

	/**
	 * {@inheritDoc}
	 */
	protected $fillable = ['item', 'value'];

	/**
	 * {@inheritDoc}
	 */
	protected $casts = ['value' => 'array'];
}
