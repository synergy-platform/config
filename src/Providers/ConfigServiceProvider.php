<?php

namespace Synergy\Config\Providers;

use PDOException;
use Synergy\Config\Repository;
use Synergy\Support\ServiceProvider;

/**
 * Service Provider for integrating with Laravel 5.1
 *
 * Part of the Config package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Config
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/config
 */

class ConfigServiceProvider extends ServiceProvider
{

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		$this->setupResources();
		$this->setupRepository();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->overrideConfigInstance();
	}

	/**
	 * Overrides the config instance.
	 *
	 * @return void
	 */
	protected function overrideConfigInstance()
	{
		$this->app->register('Illuminate\Cache\CacheServiceProvider');

		$repository = new Repository([], $this->app['cache']);

		$old = $this->app['config']->all();

		foreach ($old as $key => $value) {
			$repository->set($key, $value);
		}

		$this->app->instance('config', $repository);
	}


	/**
	 * Sets up the custom config repository
	 */
	protected function setupRepository()
	{
		$config = $this->app['config'];

		try {
			$config->setCacheKey(config('synergy-config.cache_key'));
			$config->setModel(config('synergy-config.model'));
			$config->setCached();
		} catch (PDOException $e) {}
	}


	/**
	 * Sets up the packages resources
	 */
	protected function setupResources()
	{
		// Publish config
		$config = realpath(__DIR__ . '/../../config/config.php');

		$this->mergeConfigFrom($config, 'synergy-config');

		$this->publishes([
			$config => config_path('synergy-config.php'),
		], 'config');

		// Publish migrations
		$migrations = realpath(__DIR__ . '/../../database/migrations');

		$this->publishes([
			$migrations => $this->app->databasePath().'/migrations',
		], 'migrations');
	}
}
