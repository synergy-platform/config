<?php

namespace Synergy\Config;

use Illuminate\Cache\CacheManager;
use Illuminate\Config\Repository as IlluminateRepository;
use Synergy\Config\Models\Config;
use Synergy\Support\Traits\Repository as RepositoryTrait;

/**
 * Extends Illuminate's built-in Repository class to add database storage
 * using Eloquent
 *
 * Part of the Config package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Config
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/synergy/config
 */

class Repository extends IlluminateRepository
{
	use RepositoryTrait;

	/**
	 * Array containing cached items
	 *
	 * @var array
	 */
	protected $cached = [];

	/**
	 * Instance of the Illuminate Cache Manager
	 *
	 * @var \Illuminate\Cache\CacheManager
	 */
	protected $cacheManager;

	/**
	 * Name of the cache key
	 *
	 * @var string
	 */
	protected $cacheKey = 'synergy.config';

	/**
	 * Name of the model to use
	 *
	 * @var string
	 */
	protected $model = 'Synergy\Config\Models\Config';

	/**
	 * Creates a new instance of the config Repository class
	 *
	 * @param array $items
	 */
	public function __construct(array $items = [], CacheManager $cacheManager)
	{
		$this->cacheManager = $cacheManager;

		parent::__construct($items);
	}

	/**
	 * Returns the config value.
	 *
	 * @param  string  $key
	 * @return string
	 */
	public function get($key, $default = null)
	{
		return $this->fetch($key) ?: parent::get($key, $default);
	}

	/**
	 * Retrieves a value from the database.
	 *
	 * @param  string  $key
	 * @return string|null
	 */
	protected function fetch($key)
	{
		if (isset($this->cached[$key])) {
			return $this->cached[$key];
		}
	}

	/**
	 * Stores the given item/value in the database
	 *
	 * @param      $key
	 * @param null $value
	 */
	public function persist($item, $value = null)
	{
		if (is_null($value)) {
			$this->createModel()->newQuery()->whereItem($item)->delete();
		} else {
			$data = ['item' => $item, 'value' => $value];

			$this->createModel()
				->updateOrCreate(['item' => $item], $data);
		}

		$this->setCached();
	}


	/**
	 * @return array
	 */
	public function getCached()
	{
		return $this->cached;
	}


	/**
	 * Se
	 * @param array $cached
	 */
	public function setCached()
	{
		$this->cacheManager->forget($this->cacheKey);

		$configs = $this->cacheManager->rememberForever($this->cacheKey, function() {
			return $this->createModel()->all();
		});

		$cached = [];

		foreach ($configs as $config) {

			$cached[$config->item] = $config->value;

			parent::set($config->item, $config->value);
		}

		$this->cached = $cached;
	}

	/**
	 * Returns Illuminate CacheManager instance
	 *
	 * @return CacheManager
	 */
	public function getCacheManager()
	{
		return $this->cacheManager;
	}


	/**
	 * Sets Illuminate CacheManager instance
	 *
	 * @param CacheManager $cacheManager
	 */
	public function setCacheManager($cacheManager)
	{
		$this->cacheManager = $cacheManager;
	}


	/**
	 * Returns the name of the cache key
	 *
	 * @return string
	 */
	public function getCacheKey()
	{
		return $this->cacheKey;
	}


	/**
	 * Sets the name of the cache key to use
	 *
	 * @param string $cacheKey
	 */
	public function setCacheKey($cacheKey)
	{
		$this->cacheKey = $cacheKey;
	}
}
