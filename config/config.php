<?php

/**
 * Part of the Config package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Config
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://github.com/shanedaniels/Config
 */

return [

	/*
    |--------------------------------------------------------------------------
    | Cache Key
    |--------------------------------------------------------------------------
    |
    | Specify the name of the key you want to use when caching config items.
    |
    */
	'cache_key' => 'synergy.config',

	/*
    |--------------------------------------------------------------------------
    | Model
    |--------------------------------------------------------------------------
    |
    | Specify the name of the model to use
    |
    */
	'model' => 'Synergy\Config\Models\Config'
];
